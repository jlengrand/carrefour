# Carrefour Scraper

A simple way to scrape all the carrefour locations in France and vizualize them

## Getting Started

### Installing

```
$ npm install
```

## Running the scraper (only the first time)

```
$ npm run scrape
```

Will populate the database with locations that are not known yet, and most importantly fetch google maps screenshots of the locations.

### Running the website

```
$ npm start
```

Should also open your browser to http:localhost:8080

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

## Authors

* **Julien Lengrand-Lambert** - [jlengrand](https://gitlab.com/jlengrand/)

## License

This project is licensed under the full rights reserved License - see the [LICENSE.md](LICENSE.md) file for details

## TODO (Maybe)

* Allow for filtering, and quick access to specific locations (instead of having to scroll a lot)