const puppeteer = require('puppeteer');
const low = require('lowdb')
const FileSync = require('lowdb/adapters/FileSync')
const fs = require('fs');

const adapter = new FileSync('db.json')
const db = low(adapter)

const locations = db.get('locations').value();
for (let location of locations) {
    const title = location.title;
    const link = location.location;

    const imagePath = `screenshots/${title}.png`.replace(/ /g, "-");

    //Save data
    db.get('locations')
        .find({ title: title })
        .assign({ imagePath: imagePath})
        .write();
    
}