const low = require('lowdb');
const FileSync = require('lowdb/adapters/FileSync');

const adapter = new FileSync('db.json');
const db = low(adapter);

const locations = db.get('locations').value();
for (let location of locations) {
    const title = location.title;
    const link = location.location;

    const mgrs = location.mgrs;
    const sentinelFeed = `http://sentinel-s2-l1c.s3-website.eu-central-1.amazonaws.com/#tiles/${mgrs.substring(0, 2)}/${mgrs.substring(2, 3)}/${mgrs.substring(3)}/`;

    //Save data
    db.get('locations')
        .find({ title: title })
        .assign({ sentinelFeed: sentinelFeed})
        .write();
    
}