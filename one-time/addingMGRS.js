const low = require('lowdb')
const FileSync = require('lowdb/adapters/FileSync')
const adapter = new FileSync('db.json')
const db = low(adapter)

const usng = require('usng/usng.js');

const locations = db.get('locations').value();
const precision = 1;//1e-6;
const converter = new usng.Converter();

for (let location of locations) {

    const latlon = location.latlon;
    const mgrs = converter.LLtoMGRS(latlon[0], latlon[1], precision);

    //Save data
    db.get('locations')
        .find({ title: location.title })
        .assign({ mgrs: mgrs})
        .write();
}