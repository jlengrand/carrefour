const puppeteer = require('puppeteer');
const low = require('lowdb')
const FileSync = require('lowdb/adapters/FileSync')
const fs = require('fs');

const adapter = new FileSync('db.json')
const db = low(adapter)

var files = fs.readdirSync('screenshots/');

for (let file1 of files) {
    fs.rename('screenshots/' + file1, 'screenshots/' + file1.replace(/ /g, '-'), function(err) {
        if ( err ) console.log('ERROR: ' + err);
    });
}