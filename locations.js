const queryString = require('query-string');
const puppeteer = require('puppeteer');
const low = require('lowdb')
const FileSync = require('lowdb/adapters/FileSync')

const adapter = new FileSync('db.json')
const db = low(adapter);
const usng = require('usng/usng.js');

const CARREFOUR_BASE = "http://www.carrefour.fr/magasin";
const CARREFOUR_LIST = CARREFOUR_BASE + "/liste-carrefour";

const precision = 1;
const converter = new usng.Converter();

function allLocationsKnown(){
    const links = db.get('links[0]').value();
    const locationsSize = db.get('locations').size().value();

    return links.length == locationsSize;
}

function hasLinks(){
    return db.get('links').size().value() > 0;
}

function stringToLatLon(latlon){
    return latlon.split(',').map((i) => {return parseFloat(i)});
}

function hasLocation(link){
    const locationSize = db.get('locations')
                            .filter({link: link})
                            .size()
                            .value();
    return locationSize > 0;
}

async function getLinks(browser){
    const page = await browser.newPage();
    await page.goto(CARREFOUR_BASE);
    //   await page.screenshot({ path: 'screenshots/carrefour.png' });
    
    const linksArray = await page.evaluate(
        () => [...document.querySelectorAll('.k4-storelist-sublist-link')].map(elem => elem.href)
    );
    
    db.get('links')
        .push(linksArray)
        .write();

    page.close();
}

async function getLocation(link, browser){
    console.log("=");
    console.log("Scraping", link);

    if (hasLocation(link)){
        console.log("Location already scraped. Moving forward");
        return
    }

    const page = await browser.newPage();
    await page.goto(link);

    const title = await page.evaluate(() => document.querySelector(".k5-pagehead_storename").textContent);
    const location = await page.evaluate(() => document.querySelector("div.k5-pagehead_bottom > a:nth-child(1)").href);
    const url = queryString.parseUrl(location);
    const latlon = stringToLatLon(url.query.daddr);

    //Get MGRS coordinates
    const mgrsCoord = converter.LLtoMGRS(48.903, 2.375, precision);
    const sentinelFeed = `http://sentinel-s2-l1c.s3-website.eu-central-1.amazonaws.com/#tiles/${mgrsCoord.substring(0, 2)}/${mgrsCoord.substring(2, 3)}/${mgrsCoord.substring(3)}/`;

    const item = {
        link : link,
        title : title,
        location : location,
        latlon : latlon,
        mgrs : mgrsCoord,
        sentinelFeed : sentinelFeed
    }

    console.log("Found", item);

    db.get('locations')
        .push(item)
        .write();

    page.close();
}

async function run() {

  // Init db if empty
  db.defaults({ links: [], locations: []})
    .write();

  const browser = await puppeteer.launch({
    headless: false
  });

  console.log("=========");

  if (!hasLinks()){
      console.log("Retrieving list of locations");
      await getLinks(browser);
  }
  else{
      console.log("List of locations already scraped. Moving forward");
  }

  console.log("=========");

  const links = db.get('links[0]').value();

  if(!allLocationsKnown()){
      console.log("Starting to retrieve locations");
      for (let link of links) {
        await getLocation(link, browser);
      }
  } 
  else{
    console.log("All locations already known. Nothing to do!");
  }

  console.log("=========");
  console.log("Finished!");

  browser.close();
}

run();  

// Very useful : https://github.com/emadehsan/thal