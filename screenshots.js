const puppeteer = require('puppeteer');
const low = require('lowdb')
const FileSync = require('lowdb/adapters/FileSync')
const fs = require('fs');

const adapter = new FileSync('db.json')
const db = low(adapter)

function createScreenshotDirectory(){
    if (!fs.existsSync('screenshots')){
        fs.mkdirSync('screenshots');
    }
}

function hasLocations(){
    return db.get('locations').size().value() > 0;
}

function hasScreenshot(title){
    return fs.existsSync(`screenshots/${title}.png`);
}

function transformUrl(link){
    // https://maps.google.fr/?daddr=50.637,2.412 to https://maps.google.fr/?ll=50.637,2.412&z=16&t=k
    return link.replace("daddr=", "ll=") + "&z=16&t=k";
}

async function clickIfExists(thePage, selector){
    if (!!(await thePage.$(selector))){
         await thePage.click(selector);
    }
    else {
        console.log(selector, "not found. Not clicking");
    }

}

async function makeScreenshot(location, browser){

    const title = location.title;
    const link = location.location;

    console.log("=");
    console.log("Scraping", title);

    if(hasScreenshot(title)){
        console.log("Screenshot already exists. Skipping");
        return
    }

    const page = await browser.newPage();
    await page.goto(transformUrl(link), {"waitUntil" : "networkidle0"});

    //Removing privacy popups
    clickIfExists(page, ".widget-consent-button-later");
    clickIfExists(page, ".section-homepage-promo-text-button");

    const imagePath = `screenshots/${title}.png`.replace(/ /g, "-");;
    await page.screenshot({ path: imagePath, fullPage: true});

    //Save data
    db.get('locations')
        .find({ title: title })
        .assign({ imagePath: imagePath})
        .write();



    page.close();
}

async function run() {
    if(!hasLocations()){
        console.log("No locations found. You need to scrape the locations first!");
        return;
    }

    const browser = await puppeteer.launch({
        headless: false
    });

    createScreenshotDirectory();

    console.log("Taking screenshots of all locations!");
    console.log("=========");

    const locations = db.get('locations').value();
    for (let location of locations) {
        await makeScreenshot(location, browser);
    }

    console.log("=========");
    console.log("Finished!");
    browser.close();

}

run();  
